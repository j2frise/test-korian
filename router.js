const express = require("express");
const router = express.Router();
const routeDocs = {
    getData: require('./routes/getData')
};

const encryptData = (req, res) => {
    console.log('middle -------------');
}

router.get('/', (req, res) => {
    res.json("Server for ANAP Project in running");
});

Object.keys(routeDocs).forEach(item => {
    router.use(routeDocs[item], encryptData);
});

router.use((req, res) => {
    res.status(404);
    res.json({
        error: "Page not found"
    });
});

module.exports = router;