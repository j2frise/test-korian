const express = require('express');

const cors = require('cors');
const morgan = require('morgan');
const bodyParser = require("body-parser");
const app = express();

const port = process.env.PORT || 8000;

// app.use(morgan('combined')); 
app.use(cors()); 
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.get('/',function(req, res){
    res.setHeader('Content-Type', 'text/html');
    res.status(200).send('<h1>serveur korian en marche!!!!!!</h1>')
});

app.listen(port, () => console.log('Server app listening on port ' + port));