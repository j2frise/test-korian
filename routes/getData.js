const express = require("express");
const router = express.Router();
const { QueryTypes } = require('sequelize');
const sequelize = require("../src/database/connection");
const {privateKey, publicKey} = require('../helpers/_rsa-keys');
const { Cypher } = require('@sir_koty/cypher');

const errHandler = (err) => {
    console.log('error :', err)
}

Cypher.setPublicKey(publicKey);
Cypher.setPrivateKey(privateKey);

router.get('/getEtb', async (req, res) =>{
    const query = `SELECT distinct i.Nom_Etablissement as nomEtb, i.EtabID as idEtb
            FROM dbo.Liste l 
            join InfoEtab i on i.EtabID=l.id_etab
        `
    sequelize.query(query, {type: QueryTypes.SELECT}).then(data => {
        res.send(Cypher.encrypt(data) );
    }).catch(err => {
        console.log('erreur', err);
        resp.status(500).send(Cypher.encrypt({ "error": true, "message": "L'opération a échoué'", data: {} }));
    });
});

router.get('/getSrc', async (req, res) =>{
    const query = `SELECT distinct d.Source
            FROM [dbo].[Dictionnaire] d 
        `
    sequelize.query(query, {type: QueryTypes.SELECT}).then(data => {
        res.send(Cypher.encrypt(data) );
    }).catch(err => {
        console.log('erreur', err);
        resp.status(500).send(Cypher.encrypt({ "error": true, "message": "L'opération a échoué'", data: {} }));
    });
});

router.get('/downloadAll', async (req, res) =>{
    const query = `SELECT e.EtabID, e.Nom_Etablissement, d.ID, d.Titre, d.Source, v.valeur
            FROM Valeurs v 
            join Dictionnaire d on d.ID=v.id_anap
            join InfoEtab e on e.EtabID=v.id_etab
            where v.annee_reporting = :yearOfInterest
        `
    sequelize.query(query,
        {
          replacements: { yearOfInterest: req.query.year },
          type: QueryTypes.SELECT
        }).then(data => {
        res.send(Cypher.encrypt(data) );
    }).catch(err => {
        console.log('erreur', err);
        resp.status(500).send(Cypher.encrypt({ "error": true, "message": "L'opération a échoué'", data: {} }));
    });
});

router.get('/getDataListEtb', async (req, res) => {
    const query = `SELECT e.Nom_Etablissement, d.ID as id, d.Axe, v.id_anap as anap , d.Titre, d.Source, v.valeur
            FROM Valeurs v 
            join Dictionnaire d on d.ID=v.id_anap
            join InfoEtab e on e.EtabID=v.id_etab
            where v.annee_reporting = :yearOfInterest and v.id_etab= :etb
        `
    sequelize.query(query,
        {
          replacements: { etb: req.query.id_etb, yearOfInterest: req.query.year },
          type: QueryTypes.SELECT
        }).then(data => {
            res.send(Cypher.encrypt(data) );
        }).catch(err => {
            console.log('erreur', err);
            resp.status(500).send(Cypher.encrypt({ "error": true, "message": "L'opération a échoué'", data: {} }));
        });
});

router.get('/getDataListSrc', async (req, res) =>{
    const query = `SELECT d.Source, d.Axe, d.id, d.titre, count(valeur) as valeur
            FROM Valeurs v 
            join Dictionnaire d on d.ID=v.id_anap
            join InfoEtab e on e.EtabID=v.id_etab
            group by d.ID,d.Titre, d.axe, d.Source, v.annee_reporting
            having d.Source = :source and v.annee_reporting=:yearOfInterest
        `
    sequelize.query(query,
        {
          replacements: { source: req.query.src, yearOfInterest: req.query.year },
          type: QueryTypes.SELECT
        }).then(data => {
            res.send(Cypher.encrypt(data));
        }).catch(err => {
            console.log('erreur', err);
            resp.status(500).send(Cypher.encrypt({ "error": true, "message": "L'opération a échoué'", data: {} }));
        });
});

module.exports = router;