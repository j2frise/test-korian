var SequelizeAuto = require('sequelize-auto-models')
 
var auto = new SequelizeAuto('anap', 'ksadmin', 'koriansolutions2020!', {
    host: 'koriansolutions.database.windows.net',
    dialect: 'mssql',
    directory: './models', // prevents the program from writing to disk
    // port: 1433,
    additional: {
        timestamps: false
    },
    // tables: ['Dictionnaire']
})

auto.run(function(err) {
    if (err) throw err;
    console.log(auto.tables); // table list
    console.log(auto.foreignKeys); // foreign key list
})